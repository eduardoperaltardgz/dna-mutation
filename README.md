# Welcome to dna-mutation 🚀

> Esta es una API REST desarrollada con Node.js y NestJS que determina si una persona tiene mutaciones genéticas basándose en una secuencia de ADN.

Existe una mutación si se encuentra más de una secuencia de cuatro letras iguales, de forma `oblicua`, `horizontal` o `vertical`.

> Sin mutación:

| A   | T   | G   | C   | G   | A   |
| --- | --- | --- | --- | --- | --- |
| C   | A   | G   | T   | G   | C   |
| T   | T   | A   | T   | T   | T   |
| A   | G   | A   | C   | G   | G   |
| G   | C   | G   | T   | C   | A   |
| T   | C   | A   | C   | T   | G   |

> Con mutación:

| A   | T   | G   | C   | G   | A   |
| --- | --- | --- | --- | --- | --- |
| C   | A   | G   | T   | G   | C   |
| T   | T   | A   | T   | G   | T   |
| A   | G   | A   | A   | G   | G   |
| C   | C   | C   | C   | T   | A   |
| T   | C   | A   | C   | T   | G   |

> Ejemplo de caso con mutación:

```
String[] dna = { "ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG" };
```
## Tech Specs

- Node.js `17.0.1`
- NestJS `8.0.0`
- TypeORM `8.0.2`
- SQLite3 `5.0.2`
- Jest `27.2.5`

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Postman Collection

https://www.getpostman.com/collections/5b405e6074c244b67385

## Author

* Website: https://eduardo-rdguez.github.io/
* Twitter: [@\_eduardguez](https://twitter.com/\_eduardguez)
* Github: [@eduardo-rdguez](https://github.com/eduardo-rdguez)
* GitLab: [@eduardo-rdguez](https://gitlab.com/eduardo-rdguez)