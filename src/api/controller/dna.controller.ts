import { Body, Controller, Get, HttpStatus, Post, Res } from '@nestjs/common';
import { DnaService } from '../service/dna.service';
import { DnaDto } from '../dto/dna.dto';
import { DnaEntity } from '../entity/dna.entity';

@Controller('/mutation')
export class DnaController {
  constructor(private readonly dnaService: DnaService) {}

  @Post()
  async create(@Res() response, @Body() dnaDto: DnaDto) {
    return (await this.dnaService.create(dnaDto.dna))
      ? response.status(HttpStatus.OK).send()
      : response.status(HttpStatus.FORBIDDEN).send();
  }

  @Get()
  findAll(): Promise<DnaEntity[]> {
    return this.dnaService.findAll();
  }
}
