import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class DnaEntity {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ nullable: false })
  sequence: string;

  @Column({ name: 'has_mutation', nullable: false })
  hasMutation: boolean;
}
