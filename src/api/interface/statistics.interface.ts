export interface Statistics {
  readonly count_mutations: number;

  readonly count_not_mutations: number;

  readonly ratio: number;
}
