export interface Dna {
  readonly dna: string[];

  readonly hasMutation: boolean;
}
