export class StatisticsDto {
  count_mutations: number;

  count_not_mutations: number;

  ratio: number;
}
