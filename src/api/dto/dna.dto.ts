import { IsArray, IsBoolean } from 'class-validator';

export class DnaDto {
  @IsArray()
  readonly dna: string[];

  @IsBoolean()
  readonly hasMutation: boolean;
}
