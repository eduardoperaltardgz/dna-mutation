import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { StatisticsDto } from '../dto/statistics.dto';
import { Statistics } from '../interface/statistics.interface';
import { Repository } from 'typeorm';
import { DnaEntity } from '../entity/dna.entity';

@Injectable()
export class StatisticsService {
  constructor(
    @InjectRepository(DnaEntity)
    private dnaRespository: Repository<DnaEntity>,
  ) {}

  async find(): Promise<Statistics> {
    const dnas = await this.dnaRespository.find();
    const stadisticsDto = this.createStatisticsDto(dnas);

    return stadisticsDto;
  }

  createStatisticsDto(dnas: DnaEntity[]): StatisticsDto {
    const count_mutations = this.countMutations(dnas);
    const count_not_mutations = this.countNotMutations(dnas);

    const stadisticsDto = new StatisticsDto();
    stadisticsDto.count_mutations = count_mutations;
    stadisticsDto.count_not_mutations = count_not_mutations;
    stadisticsDto.ratio = count_mutations / count_not_mutations;

    return stadisticsDto;
  }

  countMutations(dnas: DnaEntity[]): number {
    return dnas.filter((dna) => dna.hasMutation).length;
  }

  countNotMutations(dnas: DnaEntity[]): number {
    return dnas.filter((dna) => !dna.hasMutation).length;
  }
}
