import { TypeOrmModule } from '@nestjs/typeorm';
import { DnaEntity } from '../../api/entity/dna.entity';

export const TypeOrmSqliteTestingModule = () => [
  TypeOrmModule.forRoot({
    type: 'sqlite',
    database: ':memory:',
    dropSchema: true,
    entities: [DnaEntity],
    synchronize: true,
  }),
  TypeOrmModule.forFeature([DnaEntity]),
];
