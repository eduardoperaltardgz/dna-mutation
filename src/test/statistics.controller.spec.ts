import { Test, TestingModule } from '@nestjs/testing';
import { StatisticsService } from '../api/service/statistics.service';
import { StatisticsController } from '../api/controller/statistics.controller';
import { TypeOrmSqliteTestingModule } from './util/db.util';

describe('StatisticsController', () => {
  let controller: StatisticsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [...TypeOrmSqliteTestingModule()],
      controllers: [StatisticsController],
      providers: [StatisticsService],
    }).compile();

    controller = module.get<StatisticsController>(StatisticsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
